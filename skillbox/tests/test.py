import pytest
import testinfra

def test_os(host):
    assert host.file("/etc/os-release").contains("Alpine Linux")

def test_nginx_install(host):
    nginx = host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.16.1")

